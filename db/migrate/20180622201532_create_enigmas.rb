class CreateEnigmas < ActiveRecord::Migration[5.2]
  def change
    create_table :enigmas do |t|
      t.string :image
      t.string :message
      t.string :answer

      t.timestamps
    end
  end
end
