class AddKindToExtraPages < ActiveRecord::Migration[5.2]
  def change
    add_column :extra_pages, :kind, :string
  end
end
