class CreateExtraPages < ActiveRecord::Migration[5.2]
  def change
    create_table :extra_pages do |t|
      t.references :enigma, foreign_key: true
      t.string :text
      t.string :image

      t.timestamps
    end
  end
end
