# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Admin account
User.create email:"admin@admin.admin", password:"forsafaty:notpassword"

# Default enigmas
Enigma.create image:"https://i.imgur.com/9NtPHe3.png", message:"If you want to reset your playthrough", answer:"victory", id:0
Enigma.create image:"https://i.imgur.com/dNmbBcR.png", message:"We don't want you here! GET OUT!", answer:"batman"
Enigma.create image:"https://i.imgur.com/7wZuCpQ.jpg", message:"I like enigmas", answer:"cellbit"
Enigma.create image:"https://i.imgur.com/apUKYgD.png", message:"Error message not found", answer:"theanswerisnevertheansweris404neverisanswer"

# Default extra pages
ExtraPage.create kind:"about", image:"https://i.imgur.com/6fHGVRX.png", text:"This is a rails project made by André Lucas de Sousa Pinto for the discipline of Object Orientation during the course of Software Engineering in June 2018.", enigma_id:"0"
ExtraPage.create kind:"thanks", image:"https://i.imgur.com/BbHtjC0.jpg", text:"I would especially like to thank for the excellent teacher and the help of the monitors during the semester, and in general to all who have helped me to reach where I am with focus and faith.", enigma_id:"0"
ExtraPage.create kind:"hint", image:"https://i.imgur.com/OtfRp3t.png", text:"This enigma says that you need to get out, so you was supposed to log off. If you are not logged there's a message that says \"Your first enigma answer is in log in page\" and if you observe the log in page you can see there's a batman photo for no reason", enigma_id:"1"
ExtraPage.create kind:"hint", image:"", text:"Here you have a image of cells and bitcoins, and someone likes enigmas", enigma_id:"2"
ExtraPage.create kind:"normal", image:"", text:"You're on your own here, sorry", enigma_id:"3"
ExtraPage.create kind:"avaliation", image:"", text:"This is a web rails application.\nThere's 3 models: user, enigma, extra page.\nThe application can manage user access.\nIt has a more elaborate layout.\n.\n.\nThe models are related: enigma has many extra pages and extra page belong to enigma.\nThere is permission level: only admin can edit, create and see enigma and extra page index.\nOthers gems: clearense.\nThe theme is creative, has a refined implementation and is relevant", enigma_id:"0"
