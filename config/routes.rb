Rails.application.routes.draw do
  resources :enigmas
  resources :extra_pages
  post 'enigmas/:id', controller: 'enigmas', action: 'check', as: 'check'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: "welcome#show"
end
