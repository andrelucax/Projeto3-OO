require 'test_helper'

class EnigmasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @enigma = enigmas(:one)
  end

  test "should get index" do
    get enigmas_url
    assert_response :success
  end

  test "should get new" do
    get new_enigma_url
    assert_response :success
  end

  test "should create enigma" do
    assert_difference('Enigma.count') do
      post enigmas_url, params: { enigma: { answer: @enigma.answer, image: @enigma.image, message: @enigma.message } }
    end

    assert_redirected_to enigma_url(Enigma.last)
  end

  test "should show enigma" do
    get enigma_url(@enigma)
    assert_response :success
  end

  test "should get edit" do
    get edit_enigma_url(@enigma)
    assert_response :success
  end

  test "should update enigma" do
    patch enigma_url(@enigma), params: { enigma: { answer: @enigma.answer, image: @enigma.image, message: @enigma.message } }
    assert_redirected_to enigma_url(@enigma)
  end

  test "should destroy enigma" do
    assert_difference('Enigma.count', -1) do
      delete enigma_url(@enigma)
    end

    assert_redirected_to enigmas_url
  end
end
