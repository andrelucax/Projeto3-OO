require 'test_helper'

class ExtraPagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @extra_page = extra_pages(:one)
  end

  test "should get index" do
    get extra_pages_url
    assert_response :success
  end

  test "should get new" do
    get new_extra_page_url
    assert_response :success
  end

  test "should create extra_page" do
    assert_difference('ExtraPage.count') do
      post extra_pages_url, params: { extra_page: { enigma_id: @extra_page.enigma_id, image: @extra_page.image, text: @extra_page.text } }
    end

    assert_redirected_to extra_page_url(ExtraPage.last)
  end

  test "should show extra_page" do
    get extra_page_url(@extra_page)
    assert_response :success
  end

  test "should get edit" do
    get edit_extra_page_url(@extra_page)
    assert_response :success
  end

  test "should update extra_page" do
    patch extra_page_url(@extra_page), params: { extra_page: { enigma_id: @extra_page.enigma_id, image: @extra_page.image, text: @extra_page.text } }
    assert_redirected_to extra_page_url(@extra_page)
  end

  test "should destroy extra_page" do
    assert_difference('ExtraPage.count', -1) do
      delete extra_page_url(@extra_page)
    end

    assert_redirected_to extra_pages_url
  end
end
