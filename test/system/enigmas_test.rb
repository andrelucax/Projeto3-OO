require "application_system_test_case"

class EnigmasTest < ApplicationSystemTestCase
  setup do
    @enigma = enigmas(:one)
  end

  test "visiting the index" do
    visit enigmas_url
    assert_selector "h1", text: "Enigmas"
  end

  test "creating a Enigma" do
    visit enigmas_url
    click_on "New Enigma"

    fill_in "Answer", with: @enigma.answer
    fill_in "Image", with: @enigma.image
    fill_in "Message", with: @enigma.message
    click_on "Create Enigma"

    assert_text "Enigma was successfully created"
    click_on "Back"
  end

  test "updating a Enigma" do
    visit enigmas_url
    click_on "Edit", match: :first

    fill_in "Answer", with: @enigma.answer
    fill_in "Image", with: @enigma.image
    fill_in "Message", with: @enigma.message
    click_on "Update Enigma"

    assert_text "Enigma was successfully updated"
    click_on "Back"
  end

  test "destroying a Enigma" do
    visit enigmas_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Enigma was successfully destroyed"
  end
end
