require "application_system_test_case"

class ExtraPagesTest < ApplicationSystemTestCase
  setup do
    @extra_page = extra_pages(:one)
  end

  test "visiting the index" do
    visit extra_pages_url
    assert_selector "h1", text: "Extra Pages"
  end

  test "creating a Extra page" do
    visit extra_pages_url
    click_on "New Extra Page"

    fill_in "Enigma", with: @extra_page.enigma_id
    fill_in "Image", with: @extra_page.image
    fill_in "Text", with: @extra_page.text
    click_on "Create Extra page"

    assert_text "Extra page was successfully created"
    click_on "Back"
  end

  test "updating a Extra page" do
    visit extra_pages_url
    click_on "Edit", match: :first

    fill_in "Enigma", with: @extra_page.enigma_id
    fill_in "Image", with: @extra_page.image
    fill_in "Text", with: @extra_page.text
    click_on "Update Extra page"

    assert_text "Extra page was successfully updated"
    click_on "Back"
  end

  test "destroying a Extra page" do
    visit extra_pages_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Extra page was successfully destroyed"
  end
end
