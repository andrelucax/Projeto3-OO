json.extract! enigma, :id, :image, :message, :answer, :created_at, :updated_at
json.url enigma_url(enigma, format: :json)
