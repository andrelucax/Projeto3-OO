json.extract! extra_page, :id, :text, :image, :enigma_id, :created_at, :updated_at
json.url extra_page_url(extra_page, format: :json)
