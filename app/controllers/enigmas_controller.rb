class EnigmasController < ApplicationController
  before_action :set_enigma, only: [:show, :edit, :update, :destroy]

  # GET /enigmas
  # GET /enigmas.json
  def index
    @enigmas = Enigma.all
    @extra_pages = ExtraPage.all
  end

  def check
    user_answer = params[:user_answer];
    enigma_answer = params[:enigma_answer];
    id = params[:id];
    all_enigmas = Enigma.all;
    last_enigma_id = all_enigmas.last.id.to_s;
    if user_answer == enigma_answer
      if last_enigma_id.to_i < id.to_i+1
        User.find_by_email(current_user.email).update(level: 0);
        next_number = 0
        next_page = "/enigmas/"+next_number.to_s;
        redirect_to next_page;
        # puts "\n\n\n\n\n\n\n\n VOCE VENCEU !!! \n\n\n\n\n\n\n"
      else
        User.find_by_email(current_user.email).update(level: id.to_i+1);
        # puts "\n\n\n\n\n\n\n\n " + User.find_by_email(current_user.email).level.to_s + " \n\n\n\n\n\n\n"
        next_number = id.to_i+1
        next_page = "/enigmas/"+next_number.to_s;
        redirect_to next_page;
      end
      # "window.location.href='/enigmas/<%= current_user.level %>'"
      # puts "\n\n\n                " + user_answer + enigma_answer + id + " \n\n\n"
    else
      next_number = id.to_i;
      next_page = "/enigmas/"+next_number.to_s;
      redirect_to next_page;
    end
  end

  # GET /enigmas/1
  # GET /enigmas/1.json
  def show
    @extra_pages_enigma = @enigma.extra_pages
    if (current_user.email == "admin@admin.admin")
    elsif (current_user.level != params[:id].to_i)
      redirect_back fallback_location: "/cheater.html";
    end
  end

  # GET /enigmas/new
  def new
    @enigma = Enigma.new
  end

  # GET /enigmas/1/edit
  def edit
  end

  # POST /enigmas
  # POST /enigmas.json
  def create
    @enigma = Enigma.new(enigma_params)

    respond_to do |format|
      if @enigma.save
        format.html { redirect_to @enigma, notice: 'Enigma was successfully created.' }
        format.json { render :show, status: :created, location: @enigma }
      else
        format.html { render :new }
        format.json { render json: @enigma.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /enigmas/1
  # PATCH/PUT /enigmas/1.json
  def update
    respond_to do |format|
      if @enigma.update(enigma_params)
        format.html { redirect_to @enigma, notice: 'Enigma was successfully updated.' }
        format.json { render :show, status: :ok, location: @enigma }
      else
        format.html { render :edit }
        format.json { render json: @enigma.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /enigmas/1
  # DELETE /enigmas/1.json
  def destroy
    @enigma.destroy
    respond_to do |format|
      format.html { redirect_to enigmas_url, notice: 'Enigma was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_enigma
      @enigma = Enigma.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def enigma_params
      params.require(:enigma).permit(:image, :message, :answer)
    end
end
