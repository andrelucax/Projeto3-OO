class ExtraPagesController < ApplicationController
  before_action :set_extra_page, only: [:show, :edit, :update, :destroy]

  # GET /extra_pages
  # GET /extra_pages.json
  def index
    @extra_pages = ExtraPage.all
  end

  # GET /extra_pages/1
  # GET /extra_pages/1.json
  def show
    extra_page_enigma_id = @extra_page.enigma.id
    if (current_user.email == "admin@admin.admin")
    elsif (current_user.level != extra_page_enigma_id.to_i)
      redirect_back fallback_location: "/cheater.html";
    end
  end

  # GET /extra_pages/new
  def new
    @extra_page = ExtraPage.new
  end

  # GET /extra_pages/1/edit
  def edit
  end

  # POST /extra_pages
  # POST /extra_pages.json
  def create
    @extra_page = ExtraPage.new(extra_page_params)

    respond_to do |format|
      if @extra_page.save
        format.html { redirect_to @extra_page, notice: 'Extra page was successfully created.' }
        format.json { render :show, status: :created, location: @extra_page }
      else
        format.html { render :new }
        format.json { render json: @extra_page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /extra_pages/1
  # PATCH/PUT /extra_pages/1.json
  def update
    respond_to do |format|
      if @extra_page.update(extra_page_params)
        format.html { redirect_to @extra_page, notice: 'Extra page was successfully updated.' }
        format.json { render :show, status: :ok, location: @extra_page }
      else
        format.html { render :edit }
        format.json { render json: @extra_page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /extra_pages/1
  # DELETE /extra_pages/1.json
  def destroy
    @extra_page.destroy
    respond_to do |format|
      format.html { redirect_to extra_pages_url, notice: 'Extra page was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_extra_page
      @extra_page = ExtraPage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def extra_page_params
      params.require(:extra_page).permit(:text, :image, :enigma_id, :kind)
    end
end
