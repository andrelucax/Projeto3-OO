# README

3º Exercício Prático - Orientação a Objetos 2018.1 - UnB - Gama
=========================
André Lucas de Sousa Pinto - 17/0068251

## Instalação

1. Faça o clone deste projeto com ```$ git clone https://gitlab.com/andrelucax/Projeto3-OO.git ```
2. Acesse a pasta do projeto via terminal
3. Execute os seguintes comandos ``` $ bundle install ``` ``` $ rake db:drop ``` ``` $ rake db:create ``` ``` $ rake db:migrate ``` ``` $ rake db:seed ```
4. Rode a aplicação com ``` $ rails s ```
5. Vá ao seu navegador e acesse o link pelo caminho gerado, por padrão "localhost:3000"
6. Execute as ações desejadas

## Funcionamento

**Para usuários normais**
```
	[COMO JOGAR]
	* É necessário estar logado para jogar, caso nao tenha conta acesse login, sign up e cria uma conta;
	* Para jogar basta clicar no botão "PLAY";
	* Os enigmas possuem uma ou nenhuma foto, mensagem e resposta;
	* Os enigmas possuem 0...X páginas extras que podem ser do tipo "HINT" que dão uma dica muito profunda ou "NORMAL" que são dicas necessárias para chegar a reposta;
	* O usuário deve a partir das dicas chegar a resposta e escrever no campo e em seguida apertar enter;
	* Caso a reposta esteja certa o jogador será levado ao próximo enigma, caso errada o jogador continuará no mesmo enigma;
	* O objetivo é passar por todos enigmas.
```

**Para ADMs**
```
	[COMO EDITAR]
	* ADM login: "admin@admin.admin"
	* ADM password: "forsafaty:notpassword"
	* Após logar como admin você verá a mensagem "You are on admin mode edit enigmas"
	* Se você clicar no hyper link "edit enigmas" você será levado a página de enigmas
	* Aqui você poderá criar e editar novos enigmas
	* Você também poderá criar "Extra links" para seus enigmas, que podem ser do tipo "HINT" ou "NORMAL"
	* Extra links do tipo "HINT" dão dicas profundas para o jogador chegar a resposta, não sendo necessário
	* Extra links do tipo "NORMAL" serão dicas necessárias para chegar a resposta, caso o enigma seja mais complexo
	* Enigmas possuem 0...X extra links
	* Extra links são de 1 enigma
```